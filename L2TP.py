from time import sleep
import pygame

class L2TP():

    def __init__(self, startObject, endObject, output):
        self.connectionState = False
        self.sessionState = False
        self.output = output
        self.hello = pygame.USEREVENT + 0
        self.startObject = startObject

        if(self.startObject.get_option() == 3):
            self.starterVar= "LAC: "
            self.endVar = "LNS: "
        else:
            self.starterVar= "LNS: "
            self.endVar = "LAC: "
    def connect(self):

        self.connectionState = True
        self.output.set_lista_wiadomosci(self.starterVar+"send_SCCRQ")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"send_SCCRP")
        sleep(1)
        self.output.set_lista_wiadomosci(self.starterVar+"send_SCCCN")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"ZLB_ACK")
        sleep(1)

        self.output.set_lista_wiadomosci("#Połączenie nawiązane#")


    def disconnect(self):
        self.connectionState = False
        self.output.set_lista_wiadomosci(self.starterVar+"StopCCN")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"ZLB_ACK")
        sleep(1)
        self.output.set_lista_wiadomosci("#Zakończono połączenie#")
        sleep(1)

    def establish_session(self):
        self.sessionState = True
        self.output.set_lista_wiadomosci(self.starterVar+"send_ICRQ")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"send_ICRP")
        sleep(1)
        self.output.set_lista_wiadomosci(self.starterVar+"send_ICCN")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"ZLB_ACK")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"send_OCRQ")
        sleep(1)
        self.output.set_lista_wiadomosci(self.starterVar+"send_OCRP")
        sleep(1)
        self.output.set_lista_wiadomosci(self.starterVar+"send_OCCN")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"ZLB_ACK")
        sleep(1)
        self.output.set_lista_wiadomosci("#Ustanowiono sesje#")

        pygame.time.set_timer(self.hello, 11000)

    def tearDownSession(self):
        self.sessionState = False
        self.output.set_lista_wiadomosci(self.starterVar+"send_CDN")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"ZLB_ACK")
        sleep(1)
        self.output.set_lista_wiadomosci("#Zakończono sesje#")
        sleep(1)

    def helloMethod(self):
        self.output.set_lista_wiadomosci(self.starterVar+"keepAlive")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"ZLB_ACK")
        sleep(1)
        self.output.set_lista_wiadomosci("#Podtrzymano połączenie#")
        sleep(1)

        pygame.time.set_timer(self.hello, 11000)

    def sendData(self):
        self.output.set_lista_wiadomosci(self.starterVar+"send_DATA")
        sleep(1)
        self.output.set_lista_wiadomosci(self.endVar+"ZLB_ACK")
        sleep(1)
        self.output.set_lista_wiadomosci("#Wysłano dane#")
        sleep(1)
        pygame.time.set_timer(self.hello, 11000)


    def get_self_hello(self):
        return self.hello
