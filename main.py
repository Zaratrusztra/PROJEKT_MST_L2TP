import os, sys
from time import sleep

import pygame as pg  # lazy but responsible (avoid namespace flooding)

from L2TP import L2TP
from OptionBox import OptionBox as OB
import pygame_menu
import pygame



from OutputMessage import OutputMessage
from InputDataBox import InputDataBox, inputForPC
from ObjectCharacterPC import *
from Link import Link
from netaddr import IPNetwork

# TO DO LIST
# 1 PRZECZYTAJ O MASCE BITOWEJ DLA NIEWIDZIALNEJ LINI
# 2 ZAIMPLEMENTUJ RYSOWANIE LINI ŁĄCZĄCEJ DWA OBIEKTY
pg.init()
Screen = pg.display.set_mode((1600, 1000), pygame.RESIZABLE)
output = OutputMessage(Screen)

lista_obiektow = []
# lista_wiadomosci = []
lista_laczy = []
tupleLink = ()
link = 0
selected_option = -1
list_of_l2tp_objects = []
"""
główna funkcja odpowiadająca za obsługe obiektów, pierwszy if to gdy nie ma żadnego obiektu, natomiast drugi gdy
już jakiś obiekt jest na planszy
"""


def main(Surface, listOfObject, ):
    pygame.display.set_caption('L2TP Projekt')
    if len(listOfObject) == 0:
        Player = None
        while Player == None:
            Player = game_event_loop_withoutPlayer()
        listOfObject.append(Player)
        try:
            # print(listOfObject)
            listOfObject.pop(1)
        except:
            print("a")
        if listOfObject:
            screenDivide()
            drawGrid(Screen)
            for x in listOfObject:
                print(x)
                # assert isinstance(x.update, object)
                x.update(Surface)
    else:
        game_event_loop_withPlayer(listOfObject)
        screenDivide()
        draw_grid_on_screen: object = drawGrid(Screen)
        x_start = None
        y_start = None
        x_end = None
        y_end = None

        for xx in listOfObject:

            for yy in lista_laczy:
                x_start, y_start = yy.startLinkObject.get_position()
                x_end, y_end = yy.endLinkObject.get_position()
                if x_start == None or y_start == None or x_end == None or y_end == None:
                    if xx == yy.startLinkObject:
                        x_start, y_start = xx.get_position()
                        # print("1 :", x, " " , y, " ", yy.x_end, " ", yy.y_end)
                        # yy.updateLink(Screen, x, y, yy.x_end, yy.y_end )
                        pass
                    elif (xx == yy.endLinkObject):
                        x_end, y_end = xx.get_position()
                        # print("2 :",yy.x_start, " " , yy.y_start, " ", x, " ", y)
                        # yy.updateLink(Screen, yy.x_start, yy.y_start, x, y)
                        pass

                else:
                    yy.updateLink(x_start, y_start, x_end, y_end)
            assert isinstance(xx.update, object)
            xx.update(Surface)


# funkcja dodająca nowe obiekty po wykonaniu opcji prawy klik i wybraniu z menu rodzaju obiektu
def add_new_object(option):
    MyPlayer = ObjectCharacterPC((0, 0, 75, 75), option, Screen)
    lista_obiektow.append(MyPlayer)
    MyPlayer.rect.center = Screen.get_rect().center
    lista_obiektow[-1].update(Screen)

    output.set_lista_wiadomosci("Dodano nowy obiekt")

    return False, MyPlayer


def remObjFromListaLaczy():
    lista_laczy.pop(0)


# funkcja dodająca menu dodawania nowych obiektów
def moreOption(listOfObjects):
    mpos = pygame.mouse.get_pos()
    x = mpos[0]
    y = mpos[1]
    menu_obiektow = OB(x, y, 240, 40, (150, 150, 150), (100, 200, 255), pygame.font.SysFont("Comic Sans", 30),
                       ["Dodaj PC", "Dodaj Switch", "Dodaj Router", "Client(LAC)", "Server(LNS)"])
    menu_obiektow = OB(x, y, 240, 40, (150, 150, 150), (100, 200, 255), pygame.font.SysFont("Comic Sans", 30),
                       ["Client(LAC)", "Server(LNS)"])
    whiles = True
    clock = pygame.time.Clock()
    MyPlayer = None
    while whiles == True:
        mpos = pygame.mouse.get_pos()
        clock.tick(60)
        selected_option = -1
        if ((x + 170) >= mpos[0] >= x - 10 and (y + 255) >= mpos[1] >= y - 10 and mpos[0] < (
                Screen.get_width() // (10) * 8)):
            event_list = pygame.event.get()
            selected_option = menu_obiektow.update_menu(event_list)
            menu_obiektow.draw_menu_on_screen(Screen)
            pygame.display.flip()
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()
            if (selected_option > -1):
                whiles, MyPlayer = add_new_object(selected_option)
        else:
            whiles = False
    screenDivide()
    return MyPlayer


def moreOption_edit_object(listOfObjects, i, event):
    mpos = pygame.mouse.get_pos()
    x = mpos[0]
    y = mpos[1]
    menu_obiektow = OB(x, y, 200, 40, (150, 150, 150), (100, 200, 255), pygame.font.SysFont("Comic Sans", 30),
                       ["Usuń obiekt", "Dodaj łącze", "Edytuj ustawienia"])
    whiles = True
    clock = pygame.time.Clock()
    MyPlayer = 0
    selected_option = -1
    while whiles == True:
        mpos = pygame.mouse.get_pos()
        clock.tick(60)
        # selected_option = -1
        if ((x + 220) >= mpos[0] >= x - 10 and (y + 150) >= mpos[1] >= y - 10):
            # if selected_option == -1 or selected_option == 1:
            event_list = pygame.event.get()
            selected_option = menu_obiektow.update_menu(event_list)
            menu_obiektow.draw_menu_on_screen(Screen)
            pygame.display.flip()
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()
            if (selected_option == 0):
                for x in lista_laczy:
                    if x.startLinkObject == listOfObjects[i]:
                        lista_laczy.remove(x)
                    elif x.endLinkObject == listOfObjects[i]:
                        lista_laczy.remove(x)
                    list_of_l2tp_objects.pop(0)
                listOfObjects[i].deleteObject(selected_option, i, listOfObjects)
                break
            elif (selected_option == 1):
                print("")
            elif (selected_option == 2):
                listOfObjects[i].changeSettings(selected_option, Screen)
                print(listOfObjects[i].address, ":", listOfObjects[i].port)

        elif selected_option == 1:  # obługa dodawania łączy pomiędzy obiektami
            startLinkObject = listOfObjects[i]
            xd = 0
            while xd == 0:
                for event in pg.event.get():
                    for object in listOfObjects:
                        if event.type == pg.MOUSEBUTTONDOWN:
                            if object.rect.collidepoint(
                                    event.pos) and event.button == 1:  # przesuwanie obiektem przy pomocy lewego przycisku myszy
                                endLinkObject = object
                                linkObject = Link(startLinkObject, endLinkObject, Screen, listOfObjects)
                                lista_laczy.append(linkObject)
                                whiles = False
                                xd = 1
                                startLinkObject.set_connected(True)
                                endLinkObject.set_connected(True)
                                startLinkObject.set_connected_with(endLinkObject)
                                endLinkObject.set_connected_with(startLinkObject)

        else:
            whiles = False

    return MyPlayer


# dodatkowe opcje dla przypadlu gdy połaczymy dwa obiekty ze sobą
def moreOption_startSimulation(listOfObjects, i, event):
    if (len(list_of_l2tp_objects) == 0):
        l2tpObject = L2TP(listOfObjects[i], listOfObjects[i].get_connected_with(), output)
        list_of_l2tp_objects.append(l2tpObject)
    mpos = pygame.mouse.get_pos()
    x = mpos[0]
    y = mpos[1]
    if (list_of_l2tp_objects[0].connectionState == False and list_of_l2tp_objects[0].sessionState == False):
        menu_obiektow = OB(x, y, 280, 40, (150, 150, 150), (100, 200, 255), pygame.font.SysFont("Comic Sans", 30),
                           ["Usuń obiekt", "Dodaj łącze", "Edytuj ustawienia", "Rozpocznij demonstracje"])
    elif (list_of_l2tp_objects[0].connectionState == True and list_of_l2tp_objects[0].sessionState == False):
        menu_obiektow = OB(x, y, 320, 40, (150, 150, 150), (100, 200, 255), pygame.font.SysFont("Comic Sans", 30),
                           ["Usuń obiekt", "Dodaj łącze", "Edytuj ustawienia", "Ustanów sesje", "Rozłącz"])
    elif (list_of_l2tp_objects[0].connectionState == True and list_of_l2tp_objects[0].sessionState == True):
        menu_obiektow = OB(x, y, 320, 40, (150, 150, 150), (100, 200, 255), pygame.font.SysFont("Comic Sans", 30),
                           ["Usuń obiekt", "Dodaj łącze", "Edytuj ustawienia", "Wyślij dane", "Przerwij sesje"])
    else:
        menu_obiektow = OB(x, y, 280, 40, (150, 150, 150), (100, 200, 255), pygame.font.SysFont("Comic Sans", 30),
                           ["Usuń obiekt", "Dodaj łącze", "Edytuj ustawienia", "Rozpocznij demonstracje"])
    print("xxx", list_of_l2tp_objects[0].connectionState, "  ", list_of_l2tp_objects[0].sessionState)
    whiles = True
    clock = pygame.time.Clock()
    MyPlayer = 0
    selected_option = -1
    while whiles == True:
        mpos = pygame.mouse.get_pos()
        clock.tick(60)
        # selected_option = -1
        if ((x + 300) >= mpos[0] >= x - 10 and (y + 220) >= mpos[1] >= y - 10):
            # if selected_option == -1 or selected_option == 1:
            event_list = pygame.event.get()
            selected_option = menu_obiektow.update_menu(event_list)
            menu_obiektow.draw_menu_on_screen(Screen)
            pygame.display.flip()
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    sys.exit()
            if (selected_option == 0):
                for x in lista_laczy:
                    if x.startLinkObject == listOfObjects[i]:
                        lista_laczy.remove(x)
                    elif x.endLinkObject == listOfObjects[i]:
                        lista_laczy.remove(x)
                    list_of_l2tp_objects.pop(0)
                listOfObjects[i].deleteObject(selected_option, i, listOfObjects)
                break
            elif (selected_option == 1):
                print("")


            elif (selected_option == 2):
                listOfObjects[i].changeSettings(selected_option, Screen)

            elif (selected_option == 3):
                if (list_of_l2tp_objects[0].connectionState == False):
                    try:
                        if (checkSameNetwork(listOfObjects[i].get_connected_with().address,
                                             listOfObjects[i].get_connected_with().port, listOfObjects[i].address,
                                             listOfObjects[i].port)):
                            print("dobra sieć")
                            list_of_l2tp_objects[0].connect()
                            whiles = False
                        else:
                            output.set_lista_wiadomosci("PODANO BLEDNY ADRES LUB PORT!!!!")
                    except:
                        x = 0
                        for x in range(10):
                            output.set_lista_wiadomosci("Nieprawidłowy format danych")
                            x = x + 1
                            list_of_l2tp_objects[0].connectionState = False
                            list_of_l2tp_objects[0].sessionState = False


                elif (list_of_l2tp_objects[0].connectionState == True and list_of_l2tp_objects[
                    0].sessionState == False):
                    list_of_l2tp_objects[0].establish_session()
                    whiles = False
                elif (list_of_l2tp_objects[0].connectionState == True and list_of_l2tp_objects[0].sessionState == True):
                    list_of_l2tp_objects[0].sendData()
                    whiles = False

            elif (selected_option == 4):
                if (list_of_l2tp_objects[0].connectionState == True and list_of_l2tp_objects[0].sessionState == True):
                    list_of_l2tp_objects[0].tearDownSession()
                    whiles = False
                else:
                    list_of_l2tp_objects[0].disconnect()
                    list_of_l2tp_objects.pop(0)
                    whiles = False

        elif selected_option == 1:  # obługa dodawania łączy pomiędzy obiektami
            startLinkObject = listOfObjects[i]
            xd = 0
            while xd == 0:
                for event in pg.event.get():
                    for object in listOfObjects:
                        if event.type == pg.MOUSEBUTTONDOWN:
                            if object.rect.collidepoint(
                                    event.pos) and event.button == 1:  # przesuwanie obiektem przy pomocy lewego przycisku myszy
                                print("łacza")
                                endLinkObject = object
                                linkObject = Link(startLinkObject, endLinkObject, Screen, listOfObjects)
                                lista_laczy.append(linkObject)
                                whiles = False
                                xd = 1
                                startLinkObject.set_connected(True)
                                endLinkObject.set_connected(True)
                                startLinkObject.set_connected_with(endLinkObject)
                                endLinkObject.set_connected_with(startLinkObject)




        else:
            whiles = False

    return MyPlayer


def collsionWithBorder(object, event, ):
    if object.rect.collidepoint(event.pos) and event.pos[0] > (Screen.get_width() // (10) * 8):
        object.rect.move_ip(0, 0)
        object.rect.move_ip(-310, 0)


def game_event_loop_withPlayer(listOfObjects):
    new_player = 0
    collid = []
    for event in pg.event.get():
        for i, object in enumerate(listOfObjects):

            # Obługa nachodzących na siebie obiektów
            for object2 in listOfObjects:
                if object2.get_position() == object.get_position() and object2 != object:
                    object2.rect.move_ip(0, -10)
                    object.rect.move_ip(100, 10)  # Automatyczne przesunięcie obiektu o sto pikseli w prawo
            if event.type == pg.MOUSEBUTTONDOWN:
                collsionWithBorder(object, event)
                if object.rect.collidepoint(
                        event.pos) and event.button == 1:  # przesuwanie obiektem przy pomocy lewego przycisku myszy
                    object.click = True

                if object.rect.collidepoint(
                        event.pos) and event.button == 3:  # włączanie większej liczby opcji po naciśnieciu prawego przycisku
                    if ((
                            object.option == 3 or object.option == 4) and object.get_connected() and (
                            object.get_connected_with().get_option() == 3 or object.get_connected_with().get_option() == 4)):  # wyswietlanie opcji rozpoczecia symulacji
                        moreOption_startSimulation(listOfObjects, i, event)
                    else:
                        moreOption_edit_object(listOfObjects, i, event)

                if event.button == 2 and not object.rect.collidepoint(
                        event.pos):  # dodawanie nowych obiektów na polu
                    # for ii, ob in enumerate(
                    #         listOfObjects):  # Niestety z jakiegoś powodu powyższy elif nie zawsze działa dlatego tu należy iterować ponownie aby działały ustawienia dla pozostałych obiektów
                    #     if object in listOfObjects and ob.rect.collidepoint(event.pos):
                    #         moreOption_edit_object(listOfObjects, ii, event)
                    #         break
                    moreOption(listOfObjects)  # dodaj nowy obiekt
                    break
            elif event.type == pg.MOUSEBUTTONUP:
                collsionWithBorder(object, event)
                object.click = False
            elif event.type == pg.QUIT:  # wyłączanie programu
                pg.quit();
                sys.exit()
        if len(list_of_l2tp_objects) > 0 and event.type == list_of_l2tp_objects[0].get_self_hello() and \
                list_of_l2tp_objects[0].sessionState == True:
            list_of_l2tp_objects[0].helloMethod()
    return new_player


def game_event_loop_withoutPlayer() -> object:
    for event in pg.event.get():
        if event.type == pg.MOUSEBUTTONDOWN and event.button == 2:
            player = moreOption(object)
            return player
        elif event.type == pg.MOUSEBUTTONUP and event.button == 1:
            print("xx")
        elif event.type == pg.QUIT:
            print("X")
            pg.quit();
            sys.exit()


def checkSameNetwork(address1, port1, address2, port2):
    if IPNetwork(address1 + "/" + port1) == IPNetwork(address2 + "/" + port2):
        return True
    else:
        return False


def screenDivide():
    width = Screen.get_width() // (10) * 8
    height = Screen.get_height()
    Screen.fill((255, 255, 215), (0, 0, width, height))

    width2 = Screen.get_width() // (10) * 2
    height2 = Screen.get_height()

    Screen.fill((250, 230, 215), (width, 0, width2, height2))
    drawGrid(Screen)


def start():
    MyClock = pg.time.Clock()
    run = 1
    screenDivide()
    drawGrid(Screen)
    pg.display.update()
    obiekty = []

    while run == 1:
        MyClock.tick(166)
        drawGrid(Screen)
        main(Screen, lista_obiektow)
        pygame.time.delay(10)
        pg.display.update()

        xx = output.displayTextAnimation()
        # sleep(0.1)


def instruction():
    HELP = "Aby zbudować nowy obiekt należy użyć środkowego przycisku myszy a następnie wybrać typ obiektu, oraz oznaczyć go lewym przyciskiem myszy. Prawym przyciskiem myszy, " \
           "klikniętym na obiekcie można edytować parametru obiektu. Lewy przycisk myszy służy do przemieszczania obiektów"

    menu.add.label(HELP, max_char=-1, font_size=20)

def autors():
    HELP = "### AUTORZY ### \n" \
           "1. Marceli Kowalczyk - Team leader\n" \
           "2. Dawid Jaros - Team member \n" \
           "3. Piotr Juszczak - Team member \n" \
           "4. Patryk Szymaniak - Team member"

    menu.add.label(HELP, max_char=-1, font_size=20)

if __name__ == "__main__":
    # os.environ['SDL_VIDEO_CENTERED'] = '1'
    print(pygame_menu.__version__)
    print(pygame.__version__)
    width = Screen.get_width() // (10) * 8
    height = Screen.get_height()
    Screen2 = pygame.Rect((0, 0), (width, height))
    menu = pygame_menu.Menu('L2TP SYMULATOR', 1598, 998,
                            theme=pygame_menu.themes.THEME_BLUE)

    menu.add.button('Rozpocznij symulacje', start)
    menu.add.button('Instrukcja obsługi', instruction)
    menu.add.button('Autorzy', autors)
    menu.add.button('Wyjśćie', pygame_menu.events.EXIT)
    menu.mainloop(Screen)
