# noinspection Pylint
import pygame as pg
import pygame
from InputDataBox import InputDataBox, inputForPC

class ObjectCharacterPC():

    def __init__(self, rect: object, option: object, screen: object) -> object:
        self.address = ""
        self.port = ""
        self.connected_with = None
        self.connected = False
        self.screen = screen
        print(type(self.screen))
        characters = 0
        self.position = (0,1)
        # if option == 0:
        #     characters = pygame.image.load("grafiki/pc.png")
        # elif option == 1:
        #     characters = pygame.image.load("grafiki/switch.png")
        # elif option == 2:
        #     characters = pygame.image.load("grafiki/router.png")
        if option == 0:
            characters = pygame.image.load("grafiki/lac.png")
            option = 3
        elif option == 1:
            characters = pygame.image.load("grafiki/lns.png")
            option = 4
        self.option = option
        self.rect = pg.Rect(rect)
        self.click = False
        self.image = pg.Surface(self.rect.size).convert()
        # self.image.fill((255,255,255))
        self.image.blit(characters, (0, 0))

    def clicked(self, m_pos):
        return self.rect.collidepoint(m_pos)
    def get_position(self):
        self.position = self.rect.x, self.rect.y
        return self.position

    def set_offset(self, m_pos):
        self.dragging = True
        m_x, m_y = m_pos
        self.offset_x = self.rect.x - m_x
        self.offset_y = self.rect.y - m_y

        return self.offset_x, self.offset_y
    def set_connected(self, variable):
        self.connected = variable
    def get_connected(self):
        return self.connected

    def set_connected_with(self, variable):
        self.connected_with = variable

    def get_connected_with(self):
        return self.connected_with

    def get_option(self):
        return self.option

    def update(self, surface):
        if self.click:
            drawGrid(self.screen)
            self.rect.center = pg.mouse.get_pos()
        # pg.draw.line(surface, (0,0,0), (0,0), (100,100))
        surface.blit(self.image, self.rect)

    # USUN OBIEKT Z PLANSZY
    def deleteObject(self, selected_option, i, delListOfObjects):

        delListOfObjects.remove(delListOfObjects[i])

        # for x in delListOfObjects:
        #     assert isinstance(x.update, object)
        #     x.update(Screen)
        # pass

    # DODAJ ŁACZE MIEDZY OBIEKTAMI
    def addLink(selected_option, i, listOfObjects):
        # print("xDDD", listOfObjects[i].rect.x)
        pass
        # link = Link(listOfObjects[i],listOfObjects[i+1])
        # print("xDDD  ",link.x.rect.x," " ,link.y.rect.x)

    # ZMIEN USTAWIENIA OBIEKTU
    def changeSettings(self, selected_option, Screen):
        # changeSet = InputDataBox()
        self.address,  self.port = inputForPC(Screen, self.address,  self.port ) # tu trzeba jakoś pozmieniać
        pass

def drawGrid(Screen: object) -> object:
    """

    :rtype: object
    """
    BLACK = (0, 0, 0)
    WHITE = (200, 200, 200)
    WINDOW_HEIGHT = 1600
    WINDOW_WIDTH = Screen.get_width() // (10) * 8
    blockSize = 20  # Set the size of the grid block
    for x_coordinate in range(0, WINDOW_WIDTH, blockSize):
        for y_coordinate in range(0, WINDOW_HEIGHT, blockSize):
            rect = pygame.Rect(x_coordinate, y_coordinate, blockSize, blockSize)
            pygame.draw.rect(Screen, WHITE, rect, 1)